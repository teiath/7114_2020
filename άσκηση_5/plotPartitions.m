% βοηθητική ρουτίνα για σχεδίαση των mf_x, mf_y

function plotPartitions(Ux, xmin, xmax, Uy, ymin, ymax, set_xnames, set_ynames, gamma, figurenumber)

set_colors = { "magenta" "green" "red" "blue" "yellow" "cyan" "black" "magenta" "green" "red" "blue" "yellow" "cyan" "black" };

figure_title = "Διαμερισμός χώρων εισόδου/εξόδου";
xplot_title = "Διαμερισμός χώρου εισόδου";
yplot_title = "Διαμερισμός χώρου εξόδου";


figure(figurenumber, "name", strcat(figure_title , strrep(sprintf("    γ=%.1f)",gamma),".",",")));
subplot(1, 3, 1);
x=linspace(xmin, xmax, columns(Ux));
p = plot( x, Ux);
axis([ xmin, xmax, -0.02, 1.02]);
title(xplot_title);
grid minor on;
set(p, "linewidth", 2);
set (gca, 'xtick',linspace(xmin, xmax, 1 + (columns(Ux)/30)));
set (gca, 'ytick',linspace(0,1,6));
box off;
for i = 1:rows(Ux)
  set(p(i), "color", char(set_colors(1, i)));
  text(xmin + (i-1)*(xmax - xmin)/(rows(Ux)-1), 1.02, set_xnames(i));
end;

subplot(1, 3, 2);
y=linspace(ymin, ymax, columns(Uy));
p = plot( y, Uy);
axis([ ymin, ymax, -0.02, 1.02]);
title(yplot_title);
grid minor on;
set(p, "linewidth", 2);
set (gca, 'ytick',linspace(ymin, ymax, 1 + (columns(Uy)/30)));
set (gca, 'ytick',linspace(0,1,6));
box off;
for i = 1:rows(Uy)
  set(p(i), "color", char(set_colors(1, i)));
  text(ymin + (i-1)*(ymax - ymin)/(rows(Uy)-1), 1.02, set_ynames(i));
end;

end;
