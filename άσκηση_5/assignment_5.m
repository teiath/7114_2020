close all; clear all; clc;


% πεδία ορισμού Ux, Uy
xmin=0; xmax=360; xNpart=7; xstepsize = 1;
ymin=-0.4; ymax=1; yNpart=5;
set_xnames = { "A1" "A2" "A3" "A4" "A5" "A6" "A7" };
set_ynames = { "NE" "ZE" "PS" "PO" "PL" };


Ux = linspace(xmin, xmax, 1+(xmax-xmin)/xstepsize);
Uy = linspace(ymin, ymax, 1+(xmax-xmin)/xstepsize);

gamma_set = [ 0.5, 0.3, 0.4, 0.6 ];
for gamma_i = 1:length(gamma_set)
  gamma = gamma_set(gamma_i);


  % διαμερισμός εισόδου/εξόδου
  % υπολογισμός ασαφών συνόλων
  [x_triA,x_triB,x_triC]=tri_MF_partition(xmin,xmax,xNpart,gamma);
  for i = 1:xNpart;
    mf_x(i,:) = tri_MF(Ux, x_triA(i), x_triB(i), x_triC(i));
  endfor


  [y_triA,y_triB,y_triC]=tri_MF_partition(ymin,ymax,yNpart,gamma);
  for i = 1:yNpart;
    mf_y(i,:) = tri_MF(Uy, y_triA(i), y_triB(i), y_triC(i));
  endfor


  % σχεδίαση διαμερισέων εισόδου/εξόδου
  plotPartitions(mf_x, xmin, xmax, mf_y, ymin, ymax, set_xnames, set_ynames, gamma, gamma_i);


  % κανόνες
  rulesN = 0;			% αριθμός κανόνων

  % R1:   if x is A1 then y is B3  (PS)
  % R2:   if x is A2 then y is B5  (PL)
  % R3:   if x is A3 then y is B4  (PO)
  % R4:   if x is A4 then y is B3  (PS)
  % R5:   if x is A5 then y is B2  (ZE)
  % R6:   if x is A6 then y is B1  (NE)
  % R7:   if x is A7 then y is B2  (ZE)

  In(1,:)=mf_x(1,:); Out(1,:)=mf_y(3,:); rulesN++;
  In(2,:)=mf_x(2,:); Out(2,:)=mf_y(5,:); rulesN++;
  In(3,:)=mf_x(3,:); Out(3,:)=mf_y(4,:); rulesN++;
  In(4,:)=mf_x(4,:); Out(4,:)=mf_y(3,:); rulesN++;
  In(5,:)=mf_x(5,:); Out(5,:)=mf_y(2,:); rulesN++;
  In(6,:)=mf_x(6,:); Out(6,:)=mf_y(1,:); rulesN++;
  In(7,:)=mf_x(7,:); Out(7,:)=mf_y(2,:); rulesN++;



  for j=1:length(Ux)
      for i=1:rulesN
          Bbar(i,:)=min(In(i,j),Out(i,:));
      end
      % Συνολική έξοδος της βάσης κανόνων =
      %            Ένωση {Bbar(κανόνα i)} για i=1:rulesN
      total=Bbar(1,:);
      for i=2:rulesN
          total=max(total,Bbar(i,:));
      end
      Ystar(j)=sum(Uy.*total)/sum(total);
  end


  % πραγματική έξοδος
  Y = 1/2 * sin(2*pi*Ux/360) + 1/2 *(sin(2*pi*(Ux+30)/360)).^2;

  % ένα μέτρο της επιτυχούς ή μη προσέγγισης της εξόδου του συστήματος
  % με την θεωρητική έξοδο: η ρίζα του αθροίσματος των τετραγώνων της διαφοράς
  error_measure = 0;
  if (length(Uy)>0)
    error_measure = ((sum( (Ystar - Y).^2 ))^0.5)/length(Uy);
  endif;



  % σχεδίαση θεωρητικής εξόδου & εξόδου συστήματος βάσης κανόνων
  subplot(1, 3, 3);
  p = plot (Uy, [Y; Ystar]);
  text(ymin,  1.02, " Σύγκριση έξοδου συστήματος κανόνων Y* με θεωρητική έξοδο Y"); 
  text(ymin+0.55*(ymax-ymin),  0.65, strrep(sprintf("απόκλιση = %.2e", error_measure),".",","));
  legend("Y","Y*");
  set(p, "linewidth", 2);
  box off;

  %printf("γ: %.2f     d: %.3e\n", gamma, error_measure);
endfor	% gamma_i
