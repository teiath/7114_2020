close all; clear all; clc;

MF_names = { "Τριγωνική" "Τραπεζοειδής" "Γκαουσιανή" };
MF_colors = { "blue" "green" "red" };


% 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% υπολογισμός πεδίων ορισμού Ux, Uy
xmin=0; xmax=50;  npoints=101;
Ux = linspace (xmin, xmax, npoints)';
xmin=0; xmax=100; npoints=101;
Uy = linspace (xmin, xmax, npoints)';


% 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% τεχνητά δεδομένα προς απεικόνιση
Y=[ tri_MF(Ux, 13,26,38)   trap_MF(Ux, 13,24,31,38)  gauss_MF(Ux,  20,8)]; 
for i = 1:3
  figure_title = strcat(char(MF_names(1,i)), " συνάρτηση συμμετοχής");
  figure(i, "name", figure_title);
  p = plot( Ux, Y(:, i));
  set(p, "color", char(MF_colors(1, i)));
  axis([ min(Ux)-2, max(Ux)+2, -0.1, 1.1]);
  legend (char(MF_names(1, i)), "location", "northeast" );
end;


% 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigma_x = 8; sigma_y = 15;


% 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m_x = 20; m_y = 60;

[X,Y] = meshgrid(Ux,Uy);

figure_title = '"το ζεύγος (x,y) είναι κοντά στο σημείο (20,60)"';
figure(4, "name", figure_title);
s = surf( X, Y, close_to(X,Y, m_x,sigma_x, m_y,sigma_y));
axis([ min(Ux)-2, max(Ux)+2, min(Uy)-2, max(Uy)+2,  -0.1, 1.1]);
xlabel('x');
ylabel('y');
zlabel(figure_title);

