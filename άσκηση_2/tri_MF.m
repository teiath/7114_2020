% Τριγωνική συνάρτηση συμμετοχής

function r = tri_MF(x, a,b,c)
  if (!(a<b)),
    error(sprintf('tri_MF: Η παράμετρος a (%g) πρέπει να είναι μικρότερη της παραμέτρου b (%g)\n', a, b));
  elseif (!(b<c)),
    error(sprintf('tri_MF: Η παράμετρος b (%g) πρέπει να είναι μικρότερη της παραμέτρου c (%g)\n', b, c));
  end
  r = max(min((x-a)/(b-a), (c-x)/(c-b)), 0);
end

