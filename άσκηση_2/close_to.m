% πόσο κοντά είναι το ζεύγος (x,y) κοντά στο σημείο (m_x, m_y)


function r = close_to(x,y, m_x,sigma_x,  m_y,sigma_y)
  if (!(sigma_x>0)),
    error(sprintf('gauss_MF: Η παράμετρος σ_x (%g) πρέπει να είναι θετική\n', sigma_x));
  end
  if (!(sigma_y>0)),
    error(sprintf('gauss_MF: Η παράμετρος σ_y (%g) πρέπει να είναι θετική\n', sigma_y));
  end

  r = min(gauss_MF(x, m_x,sigma_x), gauss_MF(y, m_y,sigma_y));
end

