% Γκαουσιανή συνάρτηση συμμετοχής

function r = gauss_MF(x, m,sigma)
  if (!(sigma>0)),
    error(sprintf('gauss_MF: Η παράμετρος σ (%g) πρέπει να είναι θετική\n', sigma));
  end
  r = exp (-0.5 * (((x-m)/sigma).^2));
end

