% βοηθητική ρουτίνα για σχεδίαση ασαφούς κανόνα
% IF x is A AND y is B  THEN z is C    και των x0,wx, y0,wy, C'

function plotRule41(figure_counter, figure_title, Ux,A, Uy,B,
	Uz, C, x0,wx, y0,wy, Ctonos)

set_colors = { "magenta" "green" "black" };

% υπόθεση τμήμα 1
set_names={"A", "A0"};
figure(figure_counter, "name", strcat("A, A0" ,figure_title));
p = bar( Ux, A);
set(p, "linewidth", 1);
set(p, "barwidth", 0.2);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Ux), max(Ux), -0.02, 1.02]);
box off;
set(p, "facecolor", char(set_colors(1, 1)));
set(p, "edgecolor", char(set_colors(1, 1)));
line([x0 x0], [0 wx], "linewidth", 1.8, "linestyle", "-",
	"color", char(set_colors(1, 2)));
legend (set_names, "location", "northeast" );
% δείξε τον βαθμό εκπλήρωσης με διακεκομμένη γραμμή
line([ min(Ux), max(Ux)], wx, "linewidth", 3, "linestyle", ":",
	"color", char(set_colors(1, length(set_colors))));
text(max(Ux)+0.1, wx, "wx");


% υπόθεση τμήμα 2
set_names={"B", "B0"};
figure(figure_counter+1, "name", strcat("B, B0" ,figure_title));
p = bar( Uy, B);
set(p, "linewidth", 1);
set(p, "barwidth", 0.2);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uy), max(Uy), -0.02, 1.02]);
box off;
set(p, "facecolor", char(set_colors(1, 1)));
set(p, "edgecolor", char(set_colors(1, 1)));
line([y0 y0], [0 wy], "linewidth", 1.8, "linestyle", "-",
	"color", char(set_colors(1, 2)));
legend (set_names, "location", "northeast" );
% δείξε τον βαθμό εκπλήρωσης με διακεκομμένη γραμμή
line([ min(Uy), max(Uy)], wy, "linewidth", 3, "linestyle", ":",
	"color", char(set_colors(1, length(set_colors))));
text(max(Uy)+0.1, wy, "wy");


% απόδοση
set_names={"C", "C'"};
figure(figure_counter+2, "name", strcat("C, C'" ,figure_title));
[hax, h1, h2] = plotyy(Uz, C , Uz, Ctonos, @bar, @bar );
axis(hax(1), [min(Uz), max(Uz), -0.02,1.02]);
axis(hax(2), [min(Uz), max(Uz), -0.02,1.02]);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
box off;
set(h1, "linewidth", 3);
set(h1, "edgecolor", char(set_colors(1)));
set(h1, "facecolor", char(set_colors(1)));
set(h1, "barwidth", 0.2);
set(h2, "edgecolor", char(set_colors(1)));
set(h2, "facecolor", char(set_colors(2)));
set(h2, "barwidth", 0.2);
set(hax(2), "linewidth", 1);
set(hax(2), "visible", "off");
set(hax(1), "xcolor", char(set_colors(length(set_colors))));
set(hax(1), "ycolor", char(set_colors(length(set_colors))));
legend (set_names, "location", "northeast" );
% δείξε τον βαθμό εκπλήρωσης με διακεκομμένη γραμμή
line([ min(Uz), max(Uz)], wx, "linewidth", 3, "linestyle", ":",
	"color", char(set_colors(1, length(set_colors))));
line([ min(Uz), max(Uz)], wy, "linewidth", 3, "linestyle", ":",
	"color", char(set_colors(1, length(set_colors))));
text(max(Uz)+0.1, wx, "wx");
text(max(Uz)+0.1, wy, "wy");


end;
