% βοηθητική ρουτίνα για σχεδίαση των μC1'(y), μC2'(y), μC'(y)

function plotX1()

set_colors = { "magenta" "green" "red" "black" };
set_names = { "C1'" "C2'" "C'" };
figure_title = "C1'  C2'  C'";
figure(101, "name", figure_title);
Uz = linspace(0,4,101);
C1tonos = ((Uz>=2).* (4 .- Uz)/4) + 0.5*(Uz<2);
C2tonos = ((Uz<=2).* (Uz)/4) + 0.5*(Uz>2);
Ctonos = max (C1tonos, C2tonos);
p = plot( Uz, [ C1tonos; C2tonos; Ctonos]);
axis([ min(Uz), max(Uz), -0.02, 1.02]);
grid minor on;
set(p, "linewidth", 2);
set(p(3), "linewidth", 3);
set (gca, 'ytick',linspace(0,1,6));
box off;
for i = 1:3
  set(p(i), "color", char(set_colors(1, i)));
end;
legend (set_names, "location", "northeast" );

end;
