close all; clear all; clc;


x1stepsize = 0.1;
x2stepsize = 0.1;
ystepsize  = 0.1;

% πεδία ορισμού Ux1, Ux2, Uy
x1min=0; x1max=3;   Ux1 = linspace (x1min, x1max, 1+(x1max-x1min)/x1stepsize)';
x2min=0; x2max=200; Ux2 = linspace (x2min, x2max, 1+(x2max-x2min)/x2stepsize)';
y2min=0; y2max=4;   Uy = linspace (y2min, y2max, 1+(y2max-y2min)/ystepsize)';

% υπολογισμός ασαφών συνόλων
A1=tri_MF(Ux1, 0,1,2);    B1=tri_MF(Ux2, 0,100,200);
A2=tri_MF(Ux1, 1,1.8,3);  B2=tri_MF(Ux2, 80,160,260);
C1=tri_MF(Uy,  0,0,4);    C2=tri_MF(Uy,  0,4,4);

% υπολογισμός βαθμών εκπλήρωσης
x10=1.5; x20=120;
% Μετατροπή του σαφούς συνόλου εισόδου (singleton) σε ασαφή συνάρτηση:
%   δημιουργείται ένα αντίγραφο της αντιστοίχου ασαφούς υπόθεσης του κανόνα
%   με σύνολο υποστήριξης το σημείο ορισμού του singleton
w11 = tri_MF(x10, 0,1,2  );  w12 = tri_MF(x20,  0,100,200);
w21 = tri_MF(x10, 1,1.8,3);  w22 = tri_MF(x20, 80,160,260);
w1 = min (w11, w12);
w2 = min (w21, w22);

% υπολογισμός επιμέρους συμπερασμάτων
C1tonos = (C1>0) .* min(w1, C1);
C2tonos = (C2>0) .* min(w2, C2);
Ctonos = max(C1tonos, C2tonos);	% C' = C1' \/ C2'


% σχεδίαση συναρτήσεων
figure_counter=1; extra_title=" (κανόνας R1)";
plotRule41(figure_counter,extra_title,
	Ux1,A1,Ux2,B1,Uy,C1,  x10,w11, x20,w12, C1tonos);
fprintf("Βαθμός εκπλήρωσης (σχήμα %d)  w1 = %.2f   %s\n",
        figure_counter, w1, extra_title);

figure_counter=4; extra_title=" (κανόνας R2)";
plotRule41(figure_counter,extra_title,
	Ux1,A2,Ux2,B2,Uy,C2,  x10,w21, x20,w22, C2tonos);
fprintf("Βαθμός εκπλήρωσης (σχήμα %d)  w2 = %.2f   %s\n",
	figure_counter, w2, extra_title);

% απεικόνιση συνολικού συμπεράσματος
figure_counter=7; extra_title=" (συνολικό συμπέρασμα C')";
figure(7, "name", extra_title);
columns_selector = 1:3;
set_colors = { "blue" "cyan" "red" "cyan" };
set_names = { "C1'" "C2'" "C" };
p = bar(Uy, [C1tonos, C2tonos, Ctonos]);
set(p, "linewidth",0.2);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uy), max(Uy), -0.02, 1.02]);
box off;
for i = columns_selector
 set(p(i), "facecolor", char(set_colors(1, i)));
 set(p(i), "edgecolor", char(set_colors(1, i)));
end;
legend (char(set_names(1, columns_selector)), "location", "northeast" );



% αποασαφοποίηση με μέθοδο COA
steps = [ 0.5, 1, 0.01, 0.0001 ];

for i = 1:length(steps)
 stepsize = steps(i);
 y2min=0; y2max=4;   Uy = linspace (y2min, y2max, 1+(y2max-y2min)/stepsize)';
 % Υπολογισμός του C'(y)
 % > μέθοδος 1:   (απευθείας υπολογισμός για την δεδομένη απλή μορφή)
 % Ctonos = 0.5 * ones(1+(y2max-y2min)/stepsize, 1);
 % > μέθοδος 2:   (με κλήση συνάρτησης)
 Ctonos = muCtonos(Uy);
 ystar = sum(Uy' * Ctonos) / sum(Ctonos);
 fprintf("Συνολική σαφής έξοδος (βήμα διακριτοποίησης %6g)\ty* = %.2f\n",
	stepsize, ystar);
end;


