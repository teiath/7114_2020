close all; clear all; clc;


% 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% πεδία ορισμού Ux, Uy
xmin=0; xmax=20;  nxpoints=101;
Ux = linspace (xmin, xmax, nxpoints)';
ymin=0; ymax=20; nypoints=101;
Uy = linspace (ymin, ymax, nypoints)';

% υπολογισμός ασαφών συνόλων
A=tri_MF(Ux, 8,12,17);
B=tri_MF(Ux, 9,11,15);
Atonos=trap_MF(Ux, 3,5,7,13);
% 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[w, Btonos] = GMP_Mamdani(A, B, Atonos);
figure_counter=1; extra_text="";
plotRule1(figure_counter, extra_text, Ux, A, Uy, B, Atonos, Btonos, w);
fprintf("Βαθμός εκπλήρωσης (σχήμα %d)  w = %.2f   %s\n",
	figure_counter, w, extra_text);



% 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Atonos=tri_MF(Ux, 8,12,17);
[w, Btonos] = GMP_Mamdani(A, B, Atonos);
figure_counter=3; extra_text="  (ταυτιζόμενα)";
plotRule1(figure_counter, extra_text, Ux, A, Uy, B, Atonos, Btonos, w);
fprintf("Βαθμός εκπλήρωσης (σχήμα %d)  w = %.2f   %s\n",
	figure_counter, w, extra_text);
%
Atonos=trap_MF(Ux, 2,4,6,8);
[w, Btonos] = GMP_Mamdani(A, B, Atonos);
figure_counter=5; extra_text="  (με ξένα σύνολα υποστήριξης)";
plotRule1(figure_counter, extra_text, Ux, A, Uy, B, Atonos, Btonos, w);
fprintf("Βαθμός εκπλήρωσης (σχήμα %d)  w = %.2f   %s\n",
	figure_counter, w, extra_text);



% 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 5.1
% πεδία ορισμού Ux, Uy
xmin=0; xmax=100;  nxpoints=1001;
Ux = linspace (xmin, xmax, nxpoints)';
ymin=0; ymax=30; nypoints=301;
Uy = linspace (ymin, ymax, nypoints)';
zmin=0; zmax=3; nzpoints=31;
Uz = linspace (zmin, zmax, nzpoints)';

% υπολογισμός ασαφών συνόλων
Sx1=tri_MF(Ux, 0, 0, 100/3);
Sx2=tri_MF(Ux, 0, 100/3, 2*100/3);
Sx3=tri_MF(Ux, 100/3, 2*100/3, 100);
Sx4=tri_MF(Ux, 2*100/3, 100, 100);

Sy1=tri_MF(Uy, 0, 0, 20);		% small
Sy2=tri_MF(Uy, 10, 30, 30);		% large

Sz1=tri_MF(Uz, 0, 0, 2);		% small
Sz2=tri_MF(Uz, 1, 3, 3);		% large


%% 5.2
% απεικόνιση εξαχθέντων συνόλων
figure_title = "μερισμός του Ux";
figure(7, "name", figure_title);
columns_selector = 1:4;
set_colors = { "green" "magenta" "blue" "cyan" };
set_names = { "Sx1" "Sx2" "Sx3" "Sx4" };
p = plot(Ux, [Sx1 Sx2 Sx3 Sx4]);
set(p, "linewidth", 2);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Ux), max(Ux), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;

figure_title = "μερισμός του Uy";
figure(8, "name", figure_title);
columns_selector = 1:2;
set_colors = { "green" "magenta" };
set_names = { "Sy1" "Sy2" };
p = plot(Uy, [Sy1 Sy2]);
set(p, "linewidth", 2);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uy), max(Uy), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;

figure_title = "μερισμός του Uz";
figure(9, "name", figure_title);
columns_selector = 1:2;
set_colors = { "blue" "cyan" };
set_names = { "Sz1" "Sz2" };
p = plot(Uz, [Sz1 Sz2]);
set(p, "linewidth", 2);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uz), max(Uz), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;


%% 5.3
B1 = Sy1 .^ 2;		% very small
C1 = Sz2 .^ 2;		% very large
%figure(101, "name", "Sy1 Sy2 B1");
%p = plot(Uy, [Sy1 Sy2 B1]);
%figure(102, "name", "Sz1 Sz2 C1");
%p = plot(Uz, [Sz1 Sz2 C1]);




% 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 6.1
A1 = Sx1;		% slow
A2 = Sx4;		% fast
B2 = Sy1;		% small
C2 = Sz1;		% small

Atonos=tri_MF(Ux, 10,30,50);
Btonos=tri_MF(Uy,  5,10,15);
% FP1: if x is A1(slow)   AND  y is B1 (very small)
% FP2: if x is A2(fast)   AND  y is B2 (small)
%%% 6.1α, 6.1β
w11 = max(min(A1, Atonos));  % βαθμός εκπλήρωσης με τελεστής σύνθεσης max-min
w12 = max(min(B1, Btonos));
w21 = max(min(A2, Atonos));
w22 = max(min(B2, Btonos));
w1 = min(w11, w12);
w2 = min(w21, w22);
fprintf("Βαθμός εκπλήρωσης κανόνα R1 = min(%.2f,%.2f) -->    w1 = %.2f\n",
	w11, w12, w1);
fprintf("Βαθμός εκπλήρωσης κανόνα R2 = min(%.2f,%.2f) -->    w2 = %.2f\n",
	w21, w22, w2);

%%% 6.1β
C1tonos = min(w1, C1);		% τελεστής συμπερασμού Mamdani
C2tonos = min(w2, C2);

%%% 6.1γ
Ctonos = max(C1tonos, C2tonos);	% C' = C1' \/ C2'
figure_counter=10; extra_text="(τελεστής συμπερασμού Mamdani)";

%%% 6.1δ
plotRule2(figure_counter,extra_text,Uz,C1,C2,C1tonos,C2tonos,Ctonos,w1,w2);


%%% 6.2
C1tonos = w1 .* C1;		% τελεστής συμπερασμού Larsen
C2tonos = w2 .* C2;
Ctonos = max(C1tonos, C2tonos);	% C' = C1' \/ C2'
figure_counter=13; extra_text="(τελεστής συμπερασμού Larsen)";
plotRule2(figure_counter,extra_text,Uz,C1,C2,C1tonos,C2tonos,Ctonos,w1,w2);

