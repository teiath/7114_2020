% βοηθητική ρουτίνα για σχεδίαση ασαφούς κανόνα
% IF x is A THEN y is B    και των A', B'

function plotRule1(figure_counter, extra_text, Ux,A,Uy,B,Atonos,Btonos,w)

set_names = { "A" "A'" };
set_colors = { "blue" "red" "black" };
figure_title = "Σύνολα Α, A'";
figure(figure_counter, "name", strcat(figure_title, extra_text));
columns_selector= 1:2;
p = plot( Ux, [ A  Atonos w*ones(length(Ux),1) ]);
set(p, "linewidth", 3);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Ux), max(Ux), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;
set(p(length(columns_selector)+1), "color", char(set_colors(1, length(columns_selector)+1)));
set(p(length(columns_selector)+1), "linewidth", 3, "linestyle", ":");
text(max(Ux)+0.1, w, "w");


set_names = { "B" "B'" };
set_colors = { "green" "magenta" "black" };
figure_title = "Σύνολα Β, Β'";
figure(figure_counter+1, "name", strcat(figure_title, extra_text));
columns_selector= 1:2;
p = plot( Uy, [ B  Btonos w*ones(length(Uy),1) ]);
set(p, "linewidth", 3);
%p = area( Uy, [Btonos   Btonos ](:, columns_selector));
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uy), max(Uy), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;
set(p(length(columns_selector)+1), "color", char(set_colors(1, length(columns_selector)+1)));
set(p(length(columns_selector)+1), "linewidth", 3, "linestyle", ":");
text(max(Uy)+0.1, w, "w");

end;
