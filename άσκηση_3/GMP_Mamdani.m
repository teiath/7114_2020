% Γενικευμένος Modus Ponens για τον ασαφή κανόνα  IF x is A THEN y is B
% με χρήση τελεστή σύνθεσης max-min και τελεστή συμπερασμού Mamdani
%
% Δοθέντων των A, B  και  A'
% επιστρέφει τον βαθμό εκπλήρωσης w και το B' 

function [w, Btonos] = GMP_Mamdani(A, B, Atonos)
  w = max(min(A, Atonos));	% βαθμός εκπλήρωσης με τελεστής σύνθεσης max-min
  Btonos = (B>0) .* min(w, B);	% με τελεστή συμπερασμού Mamdani
end;
