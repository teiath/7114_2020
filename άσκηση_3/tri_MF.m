% Τριγωνική συνάρτηση συμμετοχής

function r = tri_MF(x, a,b,c)
  if (a==c)
    error(sprintf('tri_MF: Η παράμετρος a (%g) πρέπει να είναι μικρότερη της παραμέτρου c (%g)\n', a, c));
  end;

  if (a==b && x!=a) r = b; return; end;
  if (!(a<=b)),
    error(sprintf('tri_MF: Η παράμετρος a (%g) πρέπει να είναι μικρότερη της παραμέτρου b (%g)\n', a, b));
  elseif (!(b<=c)),
    error(sprintf('tri_MF: Η παράμετρος b (%g) πρέπει να είναι μικρότερη της παραμέτρου c (%g)\n', b, c));
  end

  if (a==b)
    if (x==a) r=1; return; end;
    if ((x<a) || (x>c)) r=0; return; end;
    r = max((c-x)/(c-a), 0);
    return;
  end;

  if (b==c)
    if (x==b) r=1; return; end;
    if ((x<a) || (x>c)) r=0; return; end;
    r = max((x-a)/(c-a), 0);
    return;
  end;

  r = max(min((x-a)/(b-a), (c-x)/(c-b)), 0);
end

