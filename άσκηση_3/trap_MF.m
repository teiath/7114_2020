% Τραπεζοειδής συνάρτηση συμμετοχής

function r = trap_MF(x, a,b,c,d)
  if (!(a<b)),
    error(sprintf('trap_MF: Η παράμετρος a (%g) πρέπει να είναι μικρότερη της παραμέτρου b (%g)\n', a, b));
  elseif (!(b<c)),
    error(sprintf('trap_MF: Η παράμετρος b (%g) πρέπει να είναι μικρότερη της παραμέτρου c (%g)\n', b, c));
  elseif (!(c<d)),
    error(sprintf('trap_MF: Η παράμετρος c (%g) πρέπει να είναι μικρότερη της παραμέτρου d (%g)\n', c, d));
  end
  r = max( min( min((x-a)/(b-a), 1), (d-x)/(d-c)), 0);
end

