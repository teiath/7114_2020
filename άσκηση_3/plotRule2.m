% βοηθητική ρουτίνα για σχεδίαση εξόδου ζεύγους ασαφούς κανόνων
% IF x is A1 THEN y is C1
% IF x is A2 THEN y is C2
function plotRule2(figure_counter,extra_text,
	Uz,C1,C2,C1tonos,C2tonos,Ctonos,w1,w2)

set_names = { "C1" "C1'" };
set_colors = { "blue" "red" "black" };
figure_title = "Σύνολα C1, C1'";
figure(figure_counter, "name", strcat(figure_title, extra_text));
columns_selector= 1:2;
p = plot( Uz, [ C1  C1tonos w1*ones(length(Uz),1) ]);
set(p, "linewidth", 3);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uz), max(Uz), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;
set(p(length(columns_selector)+1), "color", char(set_colors(1, length(columns_selector)+1)));
set(p(length(columns_selector)+1), "linewidth", 3, "linestyle", ":");
text(max(Uz)+0.1, w1, "w1");


set_names = { "C2" "C2'" };
set_colors = { "blue" "red" "black" };
figure_title = "Σύνολα C2, C2'";
figure(figure_counter+1, "name", strcat(figure_title, extra_text));
columns_selector= 1:2;
p = plot( Uz, [ C2  C2tonos w2*ones(length(Uz),1) ]);
set(p, "linewidth", 3);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uz), max(Uz), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;
set(p(length(columns_selector)+1), "color", char(set_colors(1, length(columns_selector)+1)));
set(p(length(columns_selector)+1), "linewidth", 3, "linestyle", ":");
text(max(Uz)+0.1, w2, "w2");


set_names = { "C'" };
set_colors = { "green" "black" "black" };
figure_title = "Σύνολο C'";
figure(figure_counter+2, "name", strcat(figure_title, extra_text));
columns_selector= 1:1;
p = plot( Uz, [ Ctonos w1*ones(length(Uz),1) w2*ones(length(Uz),1) ]);
set(p, "linewidth", 3);
set (gca, 'ytick',linspace(0,1,6));
grid minor on;
axis([ min(Uz), max(Uz), -0.02, 1.02]);
box off;
legend (char(set_names(1, columns_selector)), "location", "northeast" );
for i = columns_selector
 set(p(i), "color", char(set_colors(1, i)));
end;
for i = 1:2
 set(p(length(columns_selector)+i), "color", char(set_colors(1, length(columns_selector)+1)));
 set(p(length(columns_selector)+i), "linewidth", 3, "linestyle", ":");
end;
text(max(Uz)+0.1, w1, "w1");
text(max(Uz)+0.1, w2, "w2");

end;
