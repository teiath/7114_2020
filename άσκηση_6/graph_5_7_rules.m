% Μεταβολή του RMSE με τις επαναλήψεις για μοντέλα [ 5 0,2 50 0,04 ] και [ 7 0,2 50 0,04 ]]]
close all;
clear all;
clc;

[x7] = textread("7rules.txt");
[x5] = textread("5rules.txt");
rows(x5)
columns(x5)
rows(x7)
columns(x7)

figure(1)
plot(x5(1:50));
hold on
plot(x7,"r");
legend (["Μοντέλο 5 κανόνων" ; "Μοντέλο 7 κανόνων"], "location", "northeast" );
xlabel("επαναλήψεις");
ylabel("RMSE");
