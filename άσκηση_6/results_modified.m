close all;
clear all;
clc;

load datumtel.dat

% The initial and final membership functions
load init_memb.dat;
load final_memb.dat;
x=size(init_memb);
no_rules=x(2);

figure(2)
title('Initial membership functions')
hold on
for i=1:no_rules
    plot(datumtel(:,1),init_memb(:,i));
    axis([0 360 0 1]);
end
figure(3)
title('Final membership functions')
hold on
for i=1:no_rules
    plot(datumtel(:,1),final_memb(:,i));
    axis([0 360 0 1]);
end

%The initial and final membership functions rule by rule
for i=1:no_rules
    figure(i+3)
    ti=sprintf('RULE %d: Blue line: Actual system,   Red line: FNN',i);
    title(ti)
    hold on
    plot(datumtel(:,1),init_memb(:,i),datumtel(:,1),final_memb(:,i),'r');
    axis([0 360 0 1]);
end



figure(1)
% The outputs of the actual plant and the FNN outputs
plot(datumtel(:,1),datumtel(:,2));
hold on
plot(datumtel(:,1),datumtel(:,3),'r');
legend (["θεωρητική έξοδος" ; "έξοδος μοντέλου"], "location", "northeast" );
xlabel("γωνία (μοίρες)");
ylabel("τιμή συνάρτησης");
