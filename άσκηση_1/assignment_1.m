close all; clear all; clc;

% συναρτήσεις τελεστών & μέτρων ασαφών συνόλων
function r = union(A, B) r = max(A, B); end
function r = intersection(A, B) r = min(A, B); end
function r = complement(A) r = 1 .- A; end
function r = cardinality(A) r = sum(A); end
function r = S(A, B) r = cardinality(intersection(A, B))/cardinality(A); end
function r = E(A, B) r = cardinality(intersection(A, B))/cardinality(union(A,B)); end



% 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% υπολογισμός πεδίου ορισμού U
xmin=-10;xmax=50;
x = ( linspace (xmin, xmax, (xmax-(xmin)+1)) )';


% 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% υπολογισμός singleton για τα τρία ασαφή σύνολα A, B, C
A = 1 ./ (1 + (((x+10)/12) .^2 ));
B = exp(- (  ((x-20).^2) / ( 2* (15^2)) ));
C = 1 ./ (1 + (((x-50)/12) .^2 ));
sets = [ A, B, C ];
colors = { "blue" "green" "red" };
% όλα τα σύνολα σε έναν ενιαίο πίνακα (για μελλοντική ομοιόμορφη επεξεργασία)
set_names = { "A" "B" "C" };



% 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%3.1
% υπολογισμός συνόλων υποστήριξης
support_limit=0.005;
support = zeros(rows(sets),columns(sets));
for col = 1:columns(sets)
  support ( find(sets(:, col) > support_limit), col ) = 1;
end;
% και απεικόνισή τους
figure(1, "name", "Σύνολα υποστήριξης");
for col = 1:columns(sets)
  subplot(3, 1, col);
  b31 = bar(x, support(:, col) .* sets(:, col), 0.1);
  set (b31,"facecolor", char(colors(1, col)));
  set (gca, 'xtick', x(find (mod(x, 5) == 0)));
  axis([ xmin-5, xmax+5, 0 , 1.3]);
  if (col == columns(sets)) xlabel('Θερμοκρασία') end;
  legend (char(set_names(1, col)), "location", "northeast" );
end;
%

%%3.2
% υπολογισμός πυρήνων
core_limit=0.95;
core = zeros(rows(sets),columns(sets));
for col = 1:columns(sets)
  core ( find(sets(:, col) > core_limit), col ) = 1;
end;
% και απεικόνισή τους
figure(2, "name", "Πυρήνες");
for col = 1:columns(sets)
  subplot(3, 1, col);
  b32 = bar(x, core(:, col) .* sets(:, col), 0.1);
  set (b32,"facecolor", char(colors(1, col)));
  set (gca, 'xtick', x(find (mod(x, 5) == 0)));
  axis([ xmin-5, xmax+5, 0 , 1.3]);
  if (col == columns(sets)) xlabel('Θερμοκρασία') end;
  legend (char(set_names(1, col)), "location", "northeast" );
end;

%%%3.3.1
% υπολογισμός συνόλων τομής-α για α=0.4
cut_0_4_limit=0.4;
cut_0_4 = zeros(rows(sets),columns(sets));
for col = 1:columns(sets)
  cut_0_4 ( find(sets(:, col) >= cut_0_4_limit), col ) = 1;
end;
% και απεικόνισή τους
figure(3, "name", "Σύνολα τομής-α για α=0.4");
for col = 1:columns(sets)
  subplot(3, 1, col);
  b331 = bar(x, cut_0_4(:, col) .* sets(:, col), 0.1);
  set (b331,"facecolor", char(colors(1, col)));
  set (gca, 'xtick', x(find (mod(x, 5) == 0)));
  axis([ xmin-5, xmax+5, 0 , 1.3]);
  if (col == columns(sets)) xlabel('Θερμοκρασία') end;
  legend (char(set_names(1, col)), "location", "northeast" );
end;
 
%%%3.3.2
% υπολογισμός συνόλων τομής-α για α=0.8
cut_0_8_limit=0.8;
cut_0_8 = zeros(rows(sets),columns(sets));
for col = 1:columns(sets)
  cut_0_8 ( find(sets(:, col) >= cut_0_8_limit), col ) = 1;
end;
% και απεικόνισή τους
figure(4, "name", "Σύνολα τομής-α για α=0.8");
for col = 1:columns(sets)
  subplot(3, 1, col);
  b332 = bar(x, cut_0_8(:, col) .* sets(:, col), 0.1);
  set (b332,"facecolor", char(colors(1, col)));
  set (gca, 'xtick', x(find (mod(x, 5) == 0)));
  axis([ xmin-5, xmax+5, 0 , 1.3]);
  if (col == columns(sets)) xlabel('Θερμοκρασία') end;
  legend (char(set_names(1, col)), "location", "northeast" );
end;



% 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% απεικόνιση των ζευγών singleton των ασαφών συνόλων A, B, C
figure_title = "Ζεύγη singleton";
figure(5, "name", figure_title);
p5 = plot(x, sets, ".");
axis([ xmin-5, xmax+5, 0 , 1.3]);
xlabel('Θερμοκρασία');
for col = 1:columns(sets)
  set(p5(col), "color", char(colors(1, col)));
end;
legend (set_names, "location", "northeast" );



% 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% απεικόνιση τομών συνόλων
%5.1
figure_title = "Τομή συνόλων A και C";
figure(6, "name", figure_title);
p6 = plot(x, intersection(A, C), ".");
set (p6, "color", "black");
legend (figure_title, "location", "northeast" );
axis([ xmin-5, xmax+5, 0 , 1.3]);
xlabel('Θερμοκρασία');
figure_title = "Τομή συνόλων C και B";
figure(7, "name", figure_title);
p7 = plot(x, intersection(C, B), ".");
set (p7, "color", "black");
legend (figure_title, "location", "northeast" );
axis([ xmin-5, xmax+5, 0 , 1.3]);
xlabel('Θερμοκρασία');

%
%5.2
% απεικόνιση ένωσης συνόλων
figure_title = "Ένωση συνόλων A και B"
figure(8, "name", figure_title);
p8 = plot(x, union(A, B), ".");
legend (figure_title, "location", "northeast" );
set (p8, "color", "black");
axis([ xmin-5, xmax+5, 0 , 1.3]);
xlabel('Θερμοκρασία');
figure_title = "Ένωση συνόλων A και C"
figure(9, "name", figure_title);
p9 = plot(x, union(A, C), ".");
legend (figure_title, "location", "northeast" );
set (p9, "color", "black");
axis([ xmin-5, xmax+5, 0 , 1.3]);
xlabel('Θερμοκρασία');

%
%5.3
% απεικόνιση συμπληρώματος του A
figure_title = "Συμπλήρωμα του A";
figure(10, "name", figure_title);
p10 = plot(x, [A, complement(A)], ".");
%set (p10, "color", "black");
set (p10(1), "color", char(colors(1, 1)));
set (p10(2), "color", char(colors(1, 3)));
legend (char({"A" figure_title}), "location", "northeast" );
axis([ xmin-5, xmax+5, 0 , 1.3]);
xlabel('Θερμοκρασία');



% 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%6.1
"Μέγεθος A/B/C: ", cardinality(sets)
%6.2
"Μέγεθος τομής συνόλων A και C", cardinality(intersection(A, C))
"Μέγεθος τομής συνόλων C και B", cardinality(intersection(C, B))
"Μέγεθος ένωσης συνόλων A και B", cardinality(union(A, B))
"Μέγεθος ένωσης συνόλων A και C", cardinality(union(A, C))
%6.3
"Μέτρο γειτονείας A και B", S(A, B)
"Μέτρο γειτονείας C και A", S(C, A)
"Μέτρο γειτονείας A και C", S(A, C)
"Μέτρο γειτονείας C και B", S(C, B)
%
"Μέτρο ομοιότητας A και B", E(A, B)
"Μέτρο ομοιότητας A και C", E(A, C)
"Μέτρο ομοιότητας B και C", E(B, C)


